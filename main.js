const { app, BrowserWindow } = require('electron');
const path = require('path');

//function for creating the app window
function createWindow(){
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences:{
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
    }
  });
  win.loadFile(path.join(__dirname, "index.html"));
  //opens browser dev tools
  win.webContents.openDevTools();
}

//run app window creation
app.on("ready", createWindow);

//open a new window for the app if no windows are open in OSX
app.on("activate", function(){
  if(BrowserWindow.getAllWindows().length === 0){
    createWindow();
  }
})

//close the app if the window is closed, unless using OSX
app.on("window-all-closed", function(){
  if(process.platform !== "darwin"){
    app.quit();
  }
})

