const fs = require("fs");
const csv = require("csv-parser");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;

//header data and set output path for CSV writer
const csvWriter = createCsvWriter({
  path: "out.csv",
  header:[
    {id: "name", title: "Name"},
    {id: "surname", title: "Surname"},
    {id: "age", title: "Age"},
    {id: "gender", title: "Gender"},
  ]
});

//parses "data.csv" file to objects
const loadBtn = document.getElementById("loadBtn");
loadBtn.onclick = e => {
  fs.createReadStream("data.csv")
  .pipe(csv())
  .on("data", (data) => {
      addRow(data);
    })
  .on("end", () => {
    console.log("success");
  });
}

//sets the objects data to html table rows
function addRow(data){
  let newRow = document.createElement("tr");

  for(const [key, value] of Object.entries(data)){
    let newCell = document.createElement("td");
    newCell.innerHTML = value;
    newRow.append(newCell);   
  }
  document.getElementById("rows").appendChild(newRow);
}

//reads data from table cells, adds them to an object array and saves the data to a CSV file
const saveBtn = document.getElementById("saveBtn");
saveBtn.onclick = e => {
  let data = [];
  const tableLength = document.getElementById("results").rows.length;
  const rowLength = document.getElementById("results").rows[0].cells.length;

  //read table header values to an array, they are used as keys in the objects
  let headerData = document.getElementById("results").rows[0].cells;
  let headerValues = [];
  for(let cell = 0; cell < rowLength; cell++){
    headerValues.push(headerData[cell].innerHTML.toLowerCase());
  }

  //read the table values cell by cell and create key-value pairs, then add them to an object
  for(let row = 1; row < tableLength; row++){
    let obj = {};    
    for(cell = 0; cell < rowLength; cell++){
      let cellsData = document.getElementById("results").rows[row].cells;
      let pair = {[headerValues[cell]]: cellsData[cell].innerHTML}
      obj = {...obj, ...pair}
    }
    data.push(obj);
  }
  
  //write the object array to a CSV file
  csvWriter
    .writeRecords(data)
    .then(() => {
      console.log("Write succesful");
    })
    
}